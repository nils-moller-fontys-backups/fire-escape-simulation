﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonHandlersStatistics : MonoBehaviour
{
    [SerializeField] private MapHandlerScriptableObject mapHandlerObject;

    [HideInInspector] public enum SortingOption
    {
        None,
        TotalPeopleIncreasing, TotalPeopleDecreasing,
        SimulationTimeIncreasing, SimulationTimeDecreasing,
        InjuriesIncreasing, InjuriesDecreasing,
        DeathsIncreasing, DeathsDecreasing,
        BurntTilesIncreasing, BurntTilesDecreasing
    }

    private List<GameObject> entries = new List<GameObject>();
    private SortingOption currentlySortedBy = SortingOption.None;

    private void Start()
    {
        foreach (GameObject entry in GameObject.FindGameObjectsWithTag("StatisticsTableEntry"))
        {
            entries.Add(entry);
        }
        SortByDeaths();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        mapHandlerObject.mapHandler = null;
    }

    private void ReloadEntryPositions()
    {
        for (int i = 0; i < entries.Count; i++)
        {
            entries[i].transform.SetSiblingIndex(i);
        }
    }

    public void SortByTotalPeople()
    {
        //sorts for biggest number of people
        if (currentlySortedBy != SortingOption.TotalPeopleDecreasing)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgTotalPeopleTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgTotalPeopleTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 > value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.TotalPeopleDecreasing;
        }
        //sorts for smallest number of people
        else
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgTotalPeopleTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgTotalPeopleTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 < value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.TotalPeopleIncreasing;
        }
        ReloadEntryPositions();
    }

    public void SortBySimulationTime()
    {
        //sorts for smallest amount of time
        if (currentlySortedBy != SortingOption.SimulationTimeIncreasing)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgSimulationTimeTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgSimulationTimeTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 > value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.SimulationTimeIncreasing;
        }
        //sorts for biggest amount of time
        else
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgSimulationTimeTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgSimulationTimeTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 < value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.SimulationTimeDecreasing;
        }
        ReloadEntryPositions();
    }

    public void SortByInjuries()
    {
        //sorts for smallest number of injuries
        if (currentlySortedBy != SortingOption.InjuriesIncreasing)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgInjuriesTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgInjuriesTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 > value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.InjuriesIncreasing;
        }
        //sorts for biggest number of injuries
        else
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgInjuriesTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgInjuriesTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 < value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.InjuriesDecreasing;
        }
        ReloadEntryPositions();
    }

    public void SortByDeaths()
    {
        //sorts for smallest number of deaths
        if (currentlySortedBy != SortingOption.DeathsIncreasing)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgDeathsTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgDeathsTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 > value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.DeathsIncreasing;
        }
        //sorts for biggest number of deaths
        else
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgDeathsTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgDeathsTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 < value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.DeathsDecreasing;
        }
        ReloadEntryPositions();
    }

    public void SortByBurntTiles()
    {
        //sorts for smallest number of burnt tiles
        if (currentlySortedBy != SortingOption.BurntTilesIncreasing)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgBurntTilesTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgBurntTilesTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 > value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.BurntTilesIncreasing;
        }
        //sorts for biggest number of burnt tiles
        else
        {
            for (int i = 0; i < entries.Count; i++)
            {
                for (int j = 0; j < entries.Count; j++)
                {
                    int value1 = int.Parse(entries[i].transform.Find("AvgBurntTilesTitleImage").GetComponentInChildren<Text>().text);
                    int value2 = int.Parse(entries[j].transform.Find("AvgBurntTilesTitleImage").GetComponentInChildren<Text>().text);

                    if (value2 < value1)
                    {
                        GameObject temp = entries[i];
                        entries[i] = entries[j];
                        entries[j] = temp;
                    }
                }
            }
            currentlySortedBy = SortingOption.BurntTilesDecreasing;
        }
        ReloadEntryPositions();
    }
}