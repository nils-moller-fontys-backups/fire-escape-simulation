﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class CGrid : MonoBehaviour
{
    public enum Direction { Left, Right, Up, Down };
     int width;
     int height;
     float cellSize;
    Vector3 originPosition;
    private int[,] gridArray;
    //private TextMesh[,] debugTextArray;


    public CGrid (int width, int height, float cellSize, GameObject groundPrefab, Vector3 originPosition, GameObject wall)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.originPosition = originPosition;

        gridArray = new int[width, height];//Declaring grid
       // debugTextArray = new TextMesh[width, height];//Debug Visual Grid creation
        //Going through the array and drawing the array
        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                gridArray[x, y] = 0;
               // debugTextArray[x,y] = UtilsClass.CreateWorldText(gridArray[x,y].ToString(), null, GetWorldPosition(x,y)+ new Vector3(cellSize,cellSize) *.5f, 8, Color.white, TextAnchor.MiddleCenter); //doesnt show text because sorting layers. will fix in future
               // Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.white, 100f);
               // Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.white, 100f);
                if (GetNeighbour(x, y, CGrid.Direction.Up) == -1 || GetNeighbour(x, y, CGrid.Direction.Down) == -1 || GetNeighbour(x, y, CGrid.Direction.Left) == -1 || GetNeighbour(x, y, CGrid.Direction.Right) == -1)
                    SetValue(x, y, 1, wall);
                else
                {
                    SetValue(x, y, 0, groundPrefab);
                }
            }
        }
        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);   
    }

    public CGrid(int width, int height, float cellSize, GameObject groundPrefab, GameObject wall)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.originPosition = new Vector3(0, 0);

        gridArray = new int[width, height];//Declaring grid
        //debugTextArray = new TextMesh[width, height];//Debug Visual Grid creation
        
        //Going through the array and drawing the array
        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                gridArray[x, y] = 0;
               // debugTextArray[x, y] = UtilsClass.CreateWorldText(gridArray[x, y].ToString(), null, GetWorldPosition(x, y) + new Vector3(cellSize, cellSize) * .5f, 8, Color.white, TextAnchor.MiddleCenter);
               // Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.white, 100f);
               // Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.white, 100f);

                if (GetNeighbour(x, y, CGrid.Direction.Up) == -1 || GetNeighbour(x, y, CGrid.Direction.Down) == -1 || GetNeighbour(x, y, CGrid.Direction.Left) == -1 || GetNeighbour(x, y, CGrid.Direction.Right) == -1)
                    SetValue(x, y, 1, wall);
                else
                {
                    SetValue(x, y, 0, groundPrefab);
                }
            }
        }
        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);
    }

    /// <summary>
    /// The public accessor for the grid values per coordinate. ONLY set this when loading a floor plan.
    /// </summary>
    public int[,] GridArray 
    { 
        get 
        { 
            return gridArray; 
        } 
        set 
        { 
            gridArray = value; 
        } 
    }
    public float CellSize   // property
    {
        get { return cellSize; }
    }
    public int Height   // property
    {
        get { return height; }
    }
    public int Width   // property
    {
        get { return width; }
    }
    public Vector2 OriginPosition
    {
        get { return originPosition; }
    }

    /// <summary>
    /// Set's the prefab and the required position
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="buildingblock"></param>
    private void SetPrefab(int x, int y, GameObject buildingblock)
    {
        GameObject temp = Instantiate<GameObject>(buildingblock);
        temp.transform.position = GetWorldPosition(x, y) + new Vector3(CellSize, CellSize) * .5f;
    }
    /// <summary>
    /// Returns the world position based on the given X and Y
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(x, y) * CellSize + originPosition;
    }
    /// <summary>
    /// Returns the X and Y of the grid based on the given world position
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void GetXY(Vector3 worldPosition, out int x, out int y)
    {
        x = Mathf.FloorToInt((worldPosition - originPosition).x / CellSize);
        y = Mathf.FloorToInt((worldPosition - originPosition).y / CellSize);
    }
    /// <summary>
    /// Changes the number value of the grid and creates a gameobject at the position a.k.a the building block
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="value"></param>
    /// <param name="buildingblock"></param>
    public void SetValue(int x, int y, int value, GameObject buildingblock)
    {
        if(GetValue(x,y) == 2)
        {
            SetPrefab(x, y, buildingblock);
            gridArray[x, y] = 0;
        }
        if (x >= 0 && y >= 0 && x < Width && y < Height &&  (value != GetValue(x,y) || (value == 0 && GetValue(x, y) == 0) )) //added gridArray[x, y] != 1 so you cant place an object on a wall
        {
            if ((GetValue(x,y) == 5 || GetValue(x,y) == 3) && value == 1)
            {
                DeleteFromGrid(x, y, buildingblock);
                SetPrefab(x, y, buildingblock);
                gridArray[x, y] = value;
            }
            if ((GetValue(x, y) == 0) || (GetValue(x, y) == 1) && ((value == 3 || value == 5 || value == 6)))
            { 
                if (GetValue(x, y) == 1 && ( value == 3 || value == 6) && !((GetNeighbour(x, y, Direction.Up) == -1) || (GetNeighbour(x, y, Direction.Down) == -1) || (GetNeighbour(x, y, Direction.Left) == -1) || (GetNeighbour(x, y, Direction.Right) == -1)))
                {
                    DeleteFromGrid(x, y, buildingblock);
                    buildingblock.transform.localScale = new Vector2(this.cellSize, this.cellSize);
                    SetPrefab(x, y, buildingblock);
                    gridArray[x, y] = value;
                    //debugTextArray[x, y].text = gridArray[x, y].ToString();
                }
                if (value != 5 && value != 3 && value != 6 )
                {
                    buildingblock.transform.localScale = new Vector2(this.cellSize, this.cellSize);
                    SetPrefab(x, y, buildingblock);
                    gridArray[x, y] = value;
                    //debugTextArray[x, y].text = gridArray[x, y].ToString();
                }
                if ((value == 5  || value == 3)&& ((GetNeighbour(x, y, Direction.Up) == -1) || (GetNeighbour(x, y, Direction.Down) == -1) || (GetNeighbour(x, y, Direction.Left) == -1) || (GetNeighbour(x, y, Direction.Right) == -1)))
                {
                    if (GetValue(x, y) == 1)
                    { DeleteFromGrid(x, y, buildingblock, value); }
                    Debug.Log("DELETED FROM GRID WALL");
                    buildingblock.transform.localScale = new Vector2(this.cellSize, this.cellSize);
                    SetPrefab(x, y, buildingblock);
                    gridArray[x, y] = value;
                    //debugTextArray[x, y].text = gridArray[x, y].ToString();
                }
            }
        }
    }

    /// <summary>
    /// Returns the value of neighbour, 
    /// returns -1 if there is no neighbour or if x and y are out of bounds
    /// </summary>
    public int GetNeighbour(int x, int y, Direction direction)
    {
        //Left
        if (direction == Direction.Left)
        if (x >= 0 && y >= 0 && x < Width && y < Height)
        {
            if (x-1 == -1)
            {
                return -1;
            }
            else
            {
                    return GetValue(x - 1, y);
            }
        }
        //Right
        if (direction == Direction.Right)
            if (x >= 0 && y >= 0 && x < Width && y < Height)
            {
                if (x + 1 >= Width)
                {
                    return -1;
                }
                else
                {
                    return GetValue(x + 1, y);
                }
            }
        //Up
        if (direction == Direction.Up)
            if (x >= 0 && y >= 0 && x < Width && y < Height)
            {
                if (y + 1 == Height)
                {
                    return -1;
                }
                else
                {
                    return GetValue(x, y + 1);
                }
            }
        //Down
        if (direction == Direction.Down)
            if (x >= 0 && y >= 0 && x < Width && y < Height)
            {
                if (y - 1 == -1)
                {
                    return -1;
                }
                else
                {
                    return GetValue(x, y - 1);
                }
            }

        return -1;// Doesn't matter because direction can't be null
    }
    /// <summary>
    /// Returns the value of neighbour, 
    /// returns -1 if there is no neighbour or if x and y are out of bounds
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <param name="direction"></param>
    /// <returns></returns>
    public int GetNeighbour(Vector3 worldPosition, Direction direction)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);
        return GetNeighbour(x, y, direction);
    }
    /// <summary>
    /// Changes the number value of the grid and creates a gameobject at the position a.k.a the building block
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <param name="value"></param>
    /// <param name="buildingblock"></param>
    public void SetValue(Vector3 worldPosition, int value, GameObject buildingblock)
    {

        GetXY(worldPosition, out int x, out int y);
        SetValue(x, y, value, buildingblock);
    }
    /// <summary>
    /// Returns the number value of a cell
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public int GetValue(int x, int y)
    {
        if (x >= 0 && y >= 0 && x < Width && y < Height)
        {
            return gridArray[x, y];
        }
        else
            return 0;
    }
    /// <summary>
    /// Returns the number value of a cell
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>
    public int GetValue(Vector3 worldPosition)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);
        return GetValue(x, y);
    }
    /// <summary>
    /// Returns true of the given parameter is inside the grid
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>
    public bool OnGrid(Vector3 worldPosition)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);
        if (x >= 0 && x < width && y >= 0 && y < height)
            return true;
        return false;
    }
    /// <summary>
    /// Trnsparent building block while selected snapping to the grid.
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <param name="Block"></param>
    public void SnapOntoGrid(Vector3 worldPosition, GameObject Block)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);
        Block.transform.position = GetWorldPosition(x, y) + new Vector3(cellSize, cellSize) * .5f;
    }
    /// <summary>
    /// Deletes a block from the certain position
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void DeleteFromGrid(int x, int y, GameObject wall)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector2(GetWorldPosition(x, y).x+cellSize/2, GetWorldPosition(x,y).y + cellSize/2), .01f /* Radius */);
        
        if (colliders.Length >= 1)
        { 
            foreach (BoxCollider2D collider in colliders)
            {
                GameObject go = collider.gameObject; 

                Debug.Log(go.name);
                if ((GetValue(x,y) == 3 || GetValue(x,y) == 6 || GetValue(x,y) == 5 ) || !((GetNeighbour(x, y, Direction.Up) == -1) || (GetNeighbour(x, y, Direction.Down) == -1) || (GetNeighbour(x, y, Direction.Left) == -1) || (GetNeighbour(x, y, Direction.Right) == -1)))
                {
                    if ((GetValue(x, y) == 5 || GetValue(x,y) == 3 ) && ((GetNeighbour(x, y, Direction.Up) == -1) || (GetNeighbour(x, y, Direction.Down) == -1) || (GetNeighbour(x, y, Direction.Left) == -1) || (GetNeighbour(x, y, Direction.Right) == -1)))
                    {
                        Destroy(go);
                        Debug.Log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                        SetValue(x, y, 1, wall);
                        Debug.Log("GETVALUE"+GetValue(x, y));
                    }
                    else
                    {
                        Destroy(go);
                        Debug.Log("Value 1");
                        SetValue(x, y, 0);
                    }
                }
            }
        }
    }
    public void DeleteFromGrid(int x, int y, GameObject wall, int replacevalue)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(new Vector2(GetWorldPosition(x, y).x + cellSize / 2, GetWorldPosition(x, y).y + cellSize / 2), .01f /* Radius */);

        if (colliders.Length >= 1)
        {
            foreach (BoxCollider2D collider in colliders)
            {
                GameObject go = collider.gameObject;

                if (GetValue(x, y) == 1 && (replacevalue == 5 || replacevalue == 3) && ((GetNeighbour(x, y, Direction.Up) == -1) || (GetNeighbour(x, y, Direction.Down) == -1) || (GetNeighbour(x, y, Direction.Left) == -1) || (GetNeighbour(x, y, Direction.Right) == -1)))
                {
                    Destroy(go);
                    Debug.Log("Value 1 wall replacevalue");
                    SetValue(x, y, 1, wall);
                    Debug.Log("GETVALUE" + GetValue(x, y));
                }
            }
        }
    }
    /// <summary>
    /// Deletes a building block from a certain position
    /// </summary>
    /// <param name="worldPosition"></param>
    public void DeleteFromGrid(Vector3 worldPosition, GameObject wall)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);

        DeleteFromGrid(x , y, wall);
    }
    /// <summary>
    /// Changes the number value of the grid, used for deleting
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="value"></param>
    private void SetValue(int x, int y, int value)
    {
        if (x >= 0 && y >= 0 && x < width && y < height && value != GetValue(x, y))
        {
            gridArray[x, y] = value;
            //debugTextArray[x, y].text = gridArray[x, y].ToString();
        }
    }
    /// <summary>
    /// Returns X and Y as random numbers within the grid
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void GetRandomGridPosition(out int x, out int y)
    {
        y = Random.Range(0, height);
        x = Random.Range(0, width);
    }
    /// <summary>
    /// Returns X and Y as random numbers within the grid only if the grid value is as same as the Value parameter
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="value"></param>
    public void GetRandomGridPosition(out int x, out int y, int value)
    {
        do
        {
            y = (int)Random.Range(0, height);
            x = (int)Random.Range(0, width);
        } while (gridArray[x, y] != value);
    }

    /// <summary>
    /// Returns a list of Vector2's with the coordinates of the blocks with blockvalue
    /// </summary>
    /// <param name="blockvalue"></param>
    /// <returns></returns>
    public List<Vector2> GetBlockLocationsXY(int blockvalue)
    {
        List<Vector2> locations = new List<Vector2>();
        for (int i = 0; i < Width; i++)
            for (int j = 0; j < Height; j++)
            {
                if (gridArray[i, j] == blockvalue)
                    locations.Add(new Vector2(i, j));
            }
        return locations;
    }

    /// <summary>
    /// Returns the worldposition of a certain block value
    /// </summary>
    /// <param name="blockvalue"></param>
    /// <returns></returns>
    public List<Vector2> GetBlockLocationsWorldPosition(int blockvalue)
    {
        List<Vector2> locations = new List<Vector2>();
        for (int i = 0; i < Width; i++)
            for (int j = 0; j < Height; j++)
            {
                if (gridArray[i, j] == blockvalue)
                    locations.Add(GetWorldPosition(i, j) + (CellSize * new Vector3(.5f, .5f)));
            }
        return locations;
    }
}
