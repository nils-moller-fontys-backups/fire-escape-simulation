﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingController : MonoBehaviour
{
    GameObject selectedblock;
    [SerializeField]CGridScriptableObject mainGrid;
    bool slideX = false;
    bool slideY = false;
    [SerializeField] GameObject Wall;
    [SerializeField] float slidesensitivity = 50f;
    float mousepositony;
    float mousepositonx;
    bool candelete = true;
    public void SelectedBlock(GameObject selectedblock)
    {
        Debug.Log("Selected!");
        this.selectedblock = selectedblock;
    }
    void Update()
    {
        if (selectedblock != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                mainGrid.grid.SetValue(MousePosition(), SelectedBlockVariable.Value, SelectedBlockVariable.BuildingBlock);
            }
            if (Input.GetMouseButton(0) )
            {   
                int x, y;
                mainGrid.grid.GetXY(MousePosition(), out x, out y);
               // mainGrid.grid.GetXY(MousePosition(), out x, out y);
               if (slideX == false && slideY == false)
                {
                    //mainGrid.grid.SetValue(MousePosition(), SelectedBlockVariable.Value, SelectedBlockVariable.BuildingBlock);
                    mousepositony = MousePosition().y;
                    mousepositonx = MousePosition().x;
                }
                if (slideX == true)
                { 
                    mainGrid.grid.SetValue(new Vector3(MousePosition().x, mousepositony), SelectedBlockVariable.Value, SelectedBlockVariable.BuildingBlock);
                }
                if (slideY == true)
                {
                    mainGrid.grid.SetValue(new Vector3(mousepositonx, MousePosition().y), SelectedBlockVariable.Value, SelectedBlockVariable.BuildingBlock);
                }
                if ((Input.GetAxis("Mouse X") < -slidesensitivity || Input.GetAxis("Mouse X") > slidesensitivity) && (slideY == false) && Input.GetKey(KeyCode.LeftShift))
                {
                    slideX = true;
                }
                if ((Input.GetAxis("Mouse Y") < -slidesensitivity || Input.GetAxis("Mouse Y") > slidesensitivity) && (slideX == false) && Input.GetKey(KeyCode.LeftShift))
                {
                    slideY = true;
                }

            }
            if (Input.GetMouseButtonUp(0))
            {
                slideY = false;
                slideX = false ;
            }
            if (mainGrid.grid.OnGrid(MousePosition()))
            {
                mainGrid.grid.SnapOntoGrid(MousePosition(), selectedblock);
            }
            else
            {
                selectedblock.transform.position = MousePosition();
            }
        }
        if (Input.GetMouseButton(1) && candelete == true)
        {
          //  Debug.Log("UP " + mainGrid.grid.GetNeighbour(MousePosition(), CGrid.Direction.Up) + "Down" + mainGrid.grid.GetNeighbour(MousePosition(), CGrid.Direction.Down) + "Left" + mainGrid.grid.GetNeighbour(MousePosition(), CGrid.Direction.Left) + "Right" + mainGrid.grid.GetNeighbour(MousePosition(), CGrid.Direction.Right));
            mainGrid.grid.DeleteFromGrid(MousePosition(), Wall);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            int x1, y1;
            mainGrid.grid.GetRandomGridPosition(out x1, out y1);
            Debug.Log("Random X and Y" + x1 + " " + y1);
        }
    }
    public void DisableDelete()
    {
        candelete = false;
    }
    public void EnableDelete()
    {
        candelete = true;
    }

    public Vector3 MousePosition()//Get the mouse position
    {
        var mousePos = Input.mousePosition;
        mousePos.z = Camera.main.transform.position.z; // select z distance from the camera because 2D
        Vector3 MousePosition = new Vector3(Camera.main.ScreenToWorldPoint(mousePos).x * -1, Camera.main.ScreenToWorldPoint(mousePos).y * -1, 0f);
        //Debug.Log(new Vector3(Camera.main.ScreenToWorldPoint(mousePos).x, Camera.main.ScreenToWorldPoint(mousePos).y, 0f));
        return MousePosition;
        
    }
}
