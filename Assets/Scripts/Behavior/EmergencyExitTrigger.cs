﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmergencyExitTrigger : MonoBehaviour
{
    [SerializeField] GameObjectListScriptableObject peopleList;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log($"Emergency exit collided with {collision.gameObject.name}");
        if (collision.CompareTag("Person"))
        {
            peopleList.List.Remove(collision.gameObject);
            Destroy(collision.gameObject);
        }
    }
}