﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour 
{
    [SerializeField] CGridScriptableObject gridObject;
    [SerializeField] MapHandlerScriptableObject mapHandlerObject;
    [SerializeField] GameObjectListScriptableObject peopleList;

    /// <summary>
    /// Spawns an object on the grid randomly, avoiding walls. Default amount is 1.
    /// </summary>
    public void SpawnRandomly(GameObject prefabToSpawn, int amount = 1)
    {
        for (int i = 0; i < amount; i++)
        {
            int x, y;
            do
            {
                gridObject.grid.GetRandomGridPosition(out x, out y);

            } while (gridObject.grid.GetValue(x, y) != 0);

            Vector2 temp = gridObject.grid.GetWorldPosition(x, y);
            Vector2 randomWorldPosition = new Vector2(temp.x + 0.5f, temp.y + 0.5f);

            GameObject newPrefab = Instantiate(prefabToSpawn);
            if (newPrefab.CompareTag("Person"))
            {
                mapHandlerObject.mapHandler.People++;
                peopleList.List.Add(newPrefab);
            }
            newPrefab.transform.position = randomWorldPosition;

            Debug.Log($"Spawned random {newPrefab.name}");
        }
    }
}
