﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

/// <summary>
/// This script will find the closest emergency exit and set that as a target.
/// </summary>
[RequireComponent(typeof(AILerp))]
public class DestinationSetter : VersionedMonoBehaviour
{
    [SerializeField] CGridScriptableObject gridObject;

    [Tooltip("The amount of time in seconds between the pathfinder trying to find the closest exit. Defaults to 1.")]
    [SerializeField] float secondsBetweenExitFinds = 1f;
    float nextFindTime = 0;

    AILerp ai;

    private void Awake()
    {
        ai = GetComponent<AILerp>();
    }

    void OnEnable()
    {
        if (ai != null)
            ai.onSearchPath += Update;
    }

    void OnDisable()
    {
        if (ai != null) 
            ai.onSearchPath -= Update;
    }

    void Update()
    {
        //update destination after every timeBetweenExitFinds seconds
        if (Time.time > nextFindTime)
        {
            ai.destination = FindClosestEmergencyExitAsTheCrowFlies();
            //SetPathToClosestEmergencyExit();
            nextFindTime = Time.time + secondsBetweenExitFinds;
        }
    }

    /// <summary>
    /// Looks at every emergency exit on the grid and decides which one is closest (as the crow flies) and returns its location.
    /// </summary>
    /// <returns></returns>
    private Vector3 FindClosestEmergencyExitAsTheCrowFlies()
    {
        //set the condition to positive infinity
        Vector3 closestTarget = Vector3.positiveInfinity;

        //for every exit in the grid
        foreach (Vector2 exit in gridObject.grid.GetBlockLocationsWorldPosition(5))
        {
            //if the distance between the GameObject this is attached to and the exit is less than the size of the closest target (which starts off at positive infinity)
            if (Vector3.Distance(transform.position, exit) < Vector3.Distance(transform.position, closestTarget))
            {
                //get the self and exit nodes in A* grid
                GraphNode self = AstarPath.active.GetNearest(transform.position, NNConstraint.Default).node;
                GraphNode target = AstarPath.active.GetNearest(exit, NNConstraint.Default).node;

                //if the path is possible
                if (PathUtilities.IsPathPossible(self, target))
                {
                    //set the new closestTarget to the exit found
                    closestTarget = exit;
                }
            }
        }
        return closestTarget;
    }
}