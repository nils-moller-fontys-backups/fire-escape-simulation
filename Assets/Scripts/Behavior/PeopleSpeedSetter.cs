﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(AILerp))]
public class PeopleSpeedSetter : MonoBehaviour
{
    [SerializeField] IntScriptableObject globalSpeedMultiplierObject;

    AILerp ai;
    int currentMultiplier;
    float baseSpeed;

    private void Awake()
    {
        ai = GetComponent<AILerp>();
        currentMultiplier = globalSpeedMultiplierObject.Value;
        baseSpeed = ai.speed;
    }

    private void Update()
    {
        if (globalSpeedMultiplierObject.Value != currentMultiplier)
        {
            ai.speed = baseSpeed * globalSpeedMultiplierObject.Value; 
            currentMultiplier = globalSpeedMultiplierObject.Value;
        }
    }
}
