﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGridTesting : MonoBehaviour
{
    [SerializeField] private CGridScriptableObject gridObject;
    [SerializeField] private GameObject groundPrefab;
    [SerializeField] private GameObject wallPrefab;
    [SerializeField] private GameObject emergencyExitPrefab;

    private void Start()
    {
        #region grid making
        gridObject.grid.SetValue(0, 0, 1, wallPrefab);
        gridObject.grid.SetValue(0, 1, 1, wallPrefab);
        gridObject.grid.SetValue(0, 2, 1, wallPrefab);
        gridObject.grid.SetValue(0, 3, 1, wallPrefab);
        gridObject.grid.SetValue(0, 4, 1, wallPrefab);
        gridObject.grid.SetValue(0, 5, 1, wallPrefab);
        gridObject.grid.SetValue(0, 6, 1, wallPrefab);
        gridObject.grid.SetValue(0, 7, 1, wallPrefab);
        gridObject.grid.SetValue(0, 8, 1, wallPrefab);
        gridObject.grid.SetValue(0, 9, 1, wallPrefab);

        gridObject.grid.SetValue(1, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(1, 1, 1, wallPrefab);
        gridObject.grid.SetValue(1, 2, 1, wallPrefab);
        gridObject.grid.SetValue(1, 3, 1, wallPrefab);
        gridObject.grid.SetValue(1, 4, 1, wallPrefab);
        gridObject.grid.SetValue(1, 5, 1, wallPrefab);
        gridObject.grid.SetValue(1, 6, 1, wallPrefab);
        gridObject.grid.SetValue(1, 7, 1, wallPrefab);
        gridObject.grid.SetValue(1, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(1, 9, 1, wallPrefab);

        gridObject.grid.SetValue(2, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(2, 1, 1, wallPrefab);
        gridObject.grid.SetValue(2, 2, 1, wallPrefab);
        gridObject.grid.SetValue(2, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(2, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(2, 5, 1, wallPrefab);
        gridObject.grid.SetValue(2, 6, 1, wallPrefab);
        gridObject.grid.SetValue(2, 7, 1, wallPrefab);
        gridObject.grid.SetValue(2, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(2, 9, 1, wallPrefab);

        gridObject.grid.SetValue(3, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(3, 1, 1, wallPrefab);
        gridObject.grid.SetValue(3, 2, 1, wallPrefab);
        gridObject.grid.SetValue(3, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(3, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(3, 5, 1, wallPrefab);
        gridObject.grid.SetValue(3, 6, 1, wallPrefab);
        gridObject.grid.SetValue(3, 7, 1, wallPrefab);
        gridObject.grid.SetValue(3, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(3, 9, 1, wallPrefab);

        gridObject.grid.SetValue(4, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(4, 1, 1, wallPrefab);
        gridObject.grid.SetValue(4, 2, 1, wallPrefab);
        gridObject.grid.SetValue(4, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(4, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(4, 5, 1, wallPrefab);
        gridObject.grid.SetValue(4, 6, 1, wallPrefab);
        gridObject.grid.SetValue(4, 7, 1, wallPrefab);
        gridObject.grid.SetValue(4, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(4, 9, 5, emergencyExitPrefab);

        gridObject.grid.SetValue(5, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(5, 1, 1, wallPrefab);
        gridObject.grid.SetValue(5, 2, 1, wallPrefab);
        gridObject.grid.SetValue(5, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(5, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(5, 5, 1, wallPrefab);
        gridObject.grid.SetValue(5, 6, 1, wallPrefab);
        gridObject.grid.SetValue(5, 7, 1, wallPrefab);
        gridObject.grid.SetValue(5, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(5, 9, 1, wallPrefab);

        gridObject.grid.SetValue(6, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(6, 1, 1, wallPrefab);*/
        gridObject.grid.SetValue(6, 2, 1, wallPrefab);
        gridObject.grid.SetValue(6, 3, 1, wallPrefab);
        gridObject.grid.SetValue(6, 4, 1, wallPrefab);
        gridObject.grid.SetValue(6, 5, 1, wallPrefab);
        gridObject.grid.SetValue(6, 6, 1, wallPrefab);
        gridObject.grid.SetValue(6, 7, 1, wallPrefab);
        gridObject.grid.SetValue(6, 8, 1, wallPrefab);
        gridObject.grid.SetValue(6, 9, 1, wallPrefab);

        gridObject.grid.SetValue(7, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(7, 1, 1, wallPrefab);
        gridObject.grid.SetValue(7, 2, 1, wallPrefab);
        gridObject.grid.SetValue(7, 3, 1, wallPrefab);
        gridObject.grid.SetValue(7, 4, 1, wallPrefab);
        gridObject.grid.SetValue(7, 5, 1, wallPrefab);
        gridObject.grid.SetValue(7, 6, 1, wallPrefab);
        gridObject.grid.SetValue(7, 7, 1, wallPrefab);
        gridObject.grid.SetValue(7, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(7, 9, 1, wallPrefab);

        gridObject.grid.SetValue(8, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(8, 1, 1, wallPrefab);
        gridObject.grid.SetValue(8, 2, 1, wallPrefab);
        gridObject.grid.SetValue(8, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(8, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(8, 5, 1, wallPrefab);
        gridObject.grid.SetValue(8, 6, 1, wallPrefab);
        gridObject.grid.SetValue(8, 7, 1, wallPrefab);
        gridObject.grid.SetValue(8, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(8, 9, 1, wallPrefab);

        gridObject.grid.SetValue(9, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(9, 1, 1, wallPrefab);
        gridObject.grid.SetValue(9, 2, 1, wallPrefab);
        gridObject.grid.SetValue(9, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(9, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(9, 5, 1, wallPrefab);
        gridObject.grid.SetValue(9, 6, 1, wallPrefab);
        gridObject.grid.SetValue(9, 7, 1, wallPrefab);
        gridObject.grid.SetValue(9, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(9, 9, 1, wallPrefab);

        gridObject.grid.SetValue(10, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(10, 1, 1, wallPrefab);
        gridObject.grid.SetValue(10, 2, 1, wallPrefab);
        gridObject.grid.SetValue(10, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(10, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(10, 5, 1, wallPrefab);
        gridObject.grid.SetValue(10, 6, 1, wallPrefab);
        gridObject.grid.SetValue(10, 7, 1, wallPrefab);
        gridObject.grid.SetValue(10, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(10, 9, 1, wallPrefab);

        gridObject.grid.SetValue(11, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(11, 1, 1, wallPrefab);*/
        gridObject.grid.SetValue(11, 2, 1, wallPrefab);
        gridObject.grid.SetValue(11, 3, 1, wallPrefab);
        gridObject.grid.SetValue(11, 4, 1, wallPrefab);
        gridObject.grid.SetValue(11, 5, 1, wallPrefab);
        gridObject.grid.SetValue(11, 6, 1, wallPrefab);
        gridObject.grid.SetValue(11, 7, 1, wallPrefab);
        gridObject.grid.SetValue(11, 8, 1, wallPrefab);
        gridObject.grid.SetValue(11, 9, 1, wallPrefab);

        gridObject.grid.SetValue(12, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(12, 1, 1, wallPrefab);
        gridObject.grid.SetValue(12, 2, 1, wallPrefab);
        gridObject.grid.SetValue(12, 3, 1, wallPrefab);
        gridObject.grid.SetValue(12, 4, 1, wallPrefab);
        gridObject.grid.SetValue(12, 5, 1, wallPrefab);
        gridObject.grid.SetValue(12, 6, 1, wallPrefab);
        gridObject.grid.SetValue(12, 7, 1, wallPrefab);
        gridObject.grid.SetValue(12, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(12, 9, 1, wallPrefab);

        gridObject.grid.SetValue(13, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(13, 1, 1, wallPrefab);
        gridObject.grid.SetValue(13, 2, 1, wallPrefab);
        gridObject.grid.SetValue(13, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(13, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(13, 5, 1, wallPrefab);
        gridObject.grid.SetValue(13, 6, 1, wallPrefab);
        gridObject.grid.SetValue(13, 7, 1, wallPrefab);
        gridObject.grid.SetValue(13, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(13, 9, 1, wallPrefab);

        gridObject.grid.SetValue(14, 0, 5, emergencyExitPrefab);
        /*gridObject.grid.SetValue(14, 1, 1, wallPrefab);
        gridObject.grid.SetValue(14, 2, 1, wallPrefab);
        gridObject.grid.SetValue(14, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(14, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(14, 5, 1, wallPrefab);
        gridObject.grid.SetValue(14, 6, 1, wallPrefab);
        gridObject.grid.SetValue(14, 7, 1, wallPrefab);
        gridObject.grid.SetValue(14, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(14, 9, 1, wallPrefab);

        gridObject.grid.SetValue(15, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(15, 1, 1, wallPrefab);
        gridObject.grid.SetValue(15, 2, 1, wallPrefab);
        gridObject.grid.SetValue(15, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(15, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(15, 5, 1, wallPrefab);
        gridObject.grid.SetValue(15, 6, 1, wallPrefab);
        gridObject.grid.SetValue(15, 7, 1, wallPrefab);
        gridObject.grid.SetValue(15, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(15, 9, 1, wallPrefab);

        gridObject.grid.SetValue(16, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(16, 1, 1, wallPrefab);
        gridObject.grid.SetValue(16, 2, 1, wallPrefab);
        gridObject.grid.SetValue(16, 3, 1, wallPrefab);*/
        gridObject.grid.SetValue(16, 4, 1, wallPrefab);/*
        gridObject.grid.SetValue(16, 5, 1, wallPrefab);
        gridObject.grid.SetValue(16, 6, 1, wallPrefab);
        gridObject.grid.SetValue(16, 7, 1, wallPrefab);
        gridObject.grid.SetValue(16, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(16, 9, 1, wallPrefab);

        gridObject.grid.SetValue(17, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(17, 1, 1, wallPrefab);*/
        gridObject.grid.SetValue(17, 2, 1, wallPrefab);
        gridObject.grid.SetValue(17, 3, 1, wallPrefab);
        gridObject.grid.SetValue(17, 4, 1, wallPrefab);
        gridObject.grid.SetValue(17, 5, 1, wallPrefab);
        gridObject.grid.SetValue(17, 6, 1, wallPrefab);
        gridObject.grid.SetValue(17, 7, 1, wallPrefab);
        gridObject.grid.SetValue(17, 8, 1, wallPrefab);
        gridObject.grid.SetValue(17, 9, 1, wallPrefab);

        gridObject.grid.SetValue(18, 0, 1, wallPrefab);
        /*gridObject.grid.SetValue(18, 1, 1, wallPrefab);
        gridObject.grid.SetValue(18, 2, 1, wallPrefab);
        gridObject.grid.SetValue(18, 3, 1, wallPrefab);
        gridObject.grid.SetValue(18, 4, 1, wallPrefab);
        gridObject.grid.SetValue(18, 5, 1, wallPrefab);
        gridObject.grid.SetValue(18, 6, 1, wallPrefab);
        gridObject.grid.SetValue(18, 7, 1, wallPrefab);
        gridObject.grid.SetValue(18, 8, 1, wallPrefab);*/
        gridObject.grid.SetValue(18, 9, 1, wallPrefab);

        gridObject.grid.SetValue(19, 0, 1, wallPrefab);
        gridObject.grid.SetValue(19, 1, 1, wallPrefab);
        gridObject.grid.SetValue(19, 2, 1, wallPrefab);
        gridObject.grid.SetValue(19, 3, 1, wallPrefab);
        gridObject.grid.SetValue(19, 4, 1, wallPrefab);
        gridObject.grid.SetValue(19, 5, 1, wallPrefab);
        gridObject.grid.SetValue(19, 6, 1, wallPrefab);
        gridObject.grid.SetValue(19, 7, 1, wallPrefab);
        gridObject.grid.SetValue(19, 8, 1, wallPrefab);
        gridObject.grid.SetValue(19, 9, 1, wallPrefab);
        #endregion
        //Camera.main.transform.position = new Vector3(grid.Width / 2, grid.Height / 2, -((grid.Width / 2) + (grid.Height / 2))); //TODO: this works but the clicking is weird. ayden has to check with vlad
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && SelectedBlockVariable.BuildingBlock != null && SelectedBlockVariable.Value != 2) // If left mouse button is clicked
        {
            gridObject.grid.SetValue(MouseClick(), SelectedBlockVariable.Value, SelectedBlockVariable.BuildingBlock);//set the value of the grid to 10 and add the sprite with the name "Wall"
        }
        
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log(gridObject.grid.GetNeighbour(MouseClick(), CGrid.Direction.Left));
        }
    }
    public Vector3 MouseClick()//Get the mouse click
    {
        var mousePos = Input.mousePosition;
        mousePos.z = Camera.main.transform.position.z; // select z distance from the camera because 2D
        Vector3 MouseClick = new Vector3(Camera.main.ScreenToWorldPoint(mousePos).x * -1, Camera.main.ScreenToWorldPoint(mousePos).y * -1, 0f);
        //Debug.Log(new Vector3(Camera.main.ScreenToWorldPoint(mousePos).x , Camera.main.ScreenToWorldPoint(mousePos).y , 0f));
        return MouseClick;
    }
}
