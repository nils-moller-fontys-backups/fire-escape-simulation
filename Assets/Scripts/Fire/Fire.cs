﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public int XCoord { set; get; }
    public int YCoord { set; get; }

    public Fire(int x, int y)
    {
        this.XCoord = x;
        this.YCoord = y;
    }
}
