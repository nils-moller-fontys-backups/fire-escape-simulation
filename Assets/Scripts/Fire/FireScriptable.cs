﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScriptable : MonoBehaviour
{
    FireController f;
    GameObject instanceScript;

    public void StopFire()
    {
        instanceScript = GameObject.FindWithTag("startSim");
        if (instanceScript != null)
        {
            f = instanceScript.GetComponent<FireController>();
            f.StopFire();
        }
    }

    public void Speed_X1()
    {
        instanceScript = GameObject.FindWithTag("startSim");
        if (instanceScript != null)
        {
            f = instanceScript.GetComponent<FireController>();
            f.StartFire_x1();
        }
    }
    public void Speed_X2()
    {
        instanceScript = GameObject.FindWithTag("startSim");
        if (instanceScript != null)
        {
            f = instanceScript.GetComponent<FireController>();
            f.StartFire_x2();
        }
    }
    public void Speed_X4()
    {
        instanceScript = GameObject.FindWithTag("startSim");
        if (instanceScript != null)
        {
            f = instanceScript.GetComponent<FireController>();
            f.StartFire_x4();
        }
    }
}
