﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Grid", menuName = "Scriptable Objects/Custom Grid")]
public class CGridScriptableObject : ScriptableObject
{
    [HideInInspector] public CGrid grid;
}
