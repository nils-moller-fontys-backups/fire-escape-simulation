﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    //for saving the savedata to a file
    public static void SaveMap(MapHandler mapHandler, string mapname) //providing a name for the map
    {
        if (!Directory.Exists("./floorplans"))
        {
            Directory.CreateDirectory("./floorplans");
        }
        BinaryFormatter formatter = new BinaryFormatter();
        string path = "./floorplans/" + mapname + ".fsimdata"; //it will automatically look for a path that will not change
                                                                             //what is called "fsimdata" here can be called anything you want
        FileStream stream = new FileStream(path, FileMode.Create);

        SaveData savedata = new SaveData(mapHandler);

        formatter.Serialize(stream, savedata);
        stream.Close();
        Debug.Log(path);
    }

    /// <summary>
    /// Loads a floorplan and its statistics based on the name of the floorplan. The rest of the folder and extentions are automatically assigned.
    /// </summary>
    /// <param name="mapname"></param>
    /// <returns></returns>
    public static SaveData LoadSaveData(string mapname)
    {
        string path = "./floorplans/" + mapname + ".fsimdata";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            formatter.Deserialize(stream);
            stream.Position = 0;
            SaveData data = formatter.Deserialize(stream) as SaveData; //here we specify that the data from the file is of the savedata type
            stream.Close();
            return data;
        }
        else
        {
            //when there is no save data in the specified path it will return an error
            Debug.LogError("Save file not found in" + path);
            return null;
        }
    }
}
