﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavePannel : MonoBehaviour
{
    [SerializeField] MapHandlerScriptableObject MapHandlerObjectbttns;
    private string mapname = "undefined";
    
    public GameObject inputField;

    //todo: add the mapname to the string at the bottom of the page
    //todo: for some  reason if the name is too long it cuts off the beginning of the string

    private void OnEnable()
    {
        if (string.IsNullOrWhiteSpace(MapHandlerObjectbttns.mapHandler.CurrentMapName))
        {
            transform.Find("CurrentMapName").GetComponent<Text>().text = "New floorplan";
        }
        else
        {
            transform.Find("CurrentMapName").GetComponent<Text>().text = transform.Find("CurrentMapName").GetComponent<Text>().text.Replace("{}", MapHandlerObjectbttns.mapHandler.CurrentMapName);
        }
    }

    private void OnDisable()
    {
        transform.Find("CurrentMapName").GetComponent<Text>().text = transform.Find("CurrentMapName").GetComponent<Text>().text = "Current filename is {} inputting same file name will overwrite";
    }

    public void SaveButton()
    {
        mapname = inputField.GetComponentInChildren<InputField>().text;
        MapHandlerObjectbttns.mapHandler.SaveMap(mapname);
        ClosePannel();
    }

    public GameObject Spannel;
    public void ClosePannel()
    {
        if (Spannel != null)
        {
            Spannel.SetActive(false);
        }
    }
}
