﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Pathfinding;
using UnityEditor;
using System;

public class ButtonHandlers : MonoBehaviour
{
    [SerializeField] MapHandlerScriptableObject MapHandlerObjectbttns;
    [SerializeField] IntScriptableObject globalSpeedMultiplier;
    [SerializeField] CGridScriptableObject gridObject;
    [SerializeField] GameObjectListScriptableObject peopleList;

    [SerializeField] GameObject floorPrefab;
    [SerializeField] GameObject personPrefab;

    [SerializeField] BuildingController bcontroller;
    [SerializeField] GameObject exitWarning;
    [SerializeField] GameObject notSavedWarning;

    RandomSpawner spawner;
    bool searchForSimulationEnd = false;
    //quick try
    //float simulationTimeInSeconds;
    //float nextAddSecondTime = 0;
    double simulationTimeInSeconds;
    double nextAddSecondTime = 0;


    private void Awake()
    {
        spawner = GetComponent<RandomSpawner>();
        MapHandlerObjectbttns.mapHandler = new MapHandler(gridObject);
    }

    private void Update()
    {
        if (searchForSimulationEnd)
        {
            if (peopleList.List.Count == 0)
            {
                FinishSimulation();
            }
            if (Time.time > nextAddSecondTime)
            {
                simulationTimeInSeconds += 1 * globalSpeedMultiplier.Value;
                nextAddSecondTime = Time.time + 1;
            }
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        //to delete mapHandlerObject
        MapHandlerObjectbttns.mapHandler = null;
    }

    public GameObject Lpannel;
    public void LoadButton()
    {
        bcontroller.SelectedBlock(null);
        if (Lpannel != null)
        {
            bcontroller.SelectedBlock(null);
            Lpannel.SetActive(true);
        }
    
    }

    public GameObject Spannel;
    public void SaveButton()
    {
        bcontroller.SelectedBlock(null);
        if (Spannel != null)
        {
            bcontroller.SelectedBlock(null);
            Spannel.SetActive(true);
            //todo: when this is called the text at the bottom of SavePannel needs to be updated
        }
    }

    public void ChangeSpeedButton(int newSpeed)
    {
        switch (newSpeed)
        {
            case 1:
                globalSpeedMultiplier.Value = 1;
                break;
            case 2:
                globalSpeedMultiplier.Value = 2;
                break;
            case 4:
                globalSpeedMultiplier.Value = 4;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Spawns amountToSpawn people randomly and places fire randomly. Will show a UI message and return if no exits are placed.
    /// </summary>
    /// <param name="amountToSpawn"></param>
    public void StartSimulationButton()
    {
        bcontroller.SelectedBlock(null);

        if (gridObject.grid.GetBlockLocationsWorldPosition(5).Count < 1)
        {
            exitWarning.SetActive(true);
            return;
        }

        simulationTimeInSeconds = 0f;
        globalSpeedMultiplier.Value = 1;

        spawner.SpawnRandomly(personPrefab, (int)FindObjectOfType<Slider>().value);
        MapHandlerObjectbttns.mapHandler.People = (int)FindObjectOfType<Slider>().value;

        DisableFloorEditorMenu();

        searchForSimulationEnd = true;

        // InvokeRepeating("StartExtendingFire", globalSpeedMultiplier.Value, globalSpeedMultiplier.Value);
        FireController f;
        GameObject instanceScript;
        instanceScript = GameObject.FindWithTag("startSim");
        if (instanceScript != null)
        {
            f = instanceScript.GetComponent<FireController>();
            f.StartNewFire();
        }

        bcontroller.DisableDelete();

        Debug.Log("Started simulation");
    }

    /// <summary>
    /// Set global speed to zero.
    /// </summary>
    public void PauseSimulation()
    {
        globalSpeedMultiplier.Value = 0;
        Debug.Log("Paused simulation");
    }

    /// <summary> 
    /// Removes all people and fire from the grid and resets stats for this run. Also enables the floor plan editor.
    /// </summary>
    public void StopSimulationButton()
    {
        searchForSimulationEnd = false;

        MapHandlerObjectbttns.mapHandler.ResetCurrentSimulationData();
        EnableFloorEditorMenu();

        foreach (AILerp person in FindObjectsOfType<AILerp>())
        {
            Destroy(person.gameObject);
        }

        foreach (Fire fire in FindObjectsOfType<Fire>())
        {
            gridObject.grid.SetValue(fire.gameObject.transform.position, 0, floorPrefab);
            Destroy(fire.gameObject);
        }

        FireController f;
        GameObject instanceScript;
        instanceScript = GameObject.FindWithTag("startSim");
        if (instanceScript != null)
        {
            f = instanceScript.GetComponent<FireController>();
            f.StopFire();
        }
        bcontroller.EnableDelete();
        Debug.Log("Simulation stopped");     
    }

    /// <summary>
    /// Removes all people and fire. Also enables the floor plan editor.
    /// </summary>
    public void FinishSimulation()
    {
        searchForSimulationEnd = false;

        EnableFloorEditorMenu();

        //add to statistics
        MapHandlerObjectbttns.mapHandler.SimulationTime = simulationTimeInSeconds;
        MapHandlerObjectbttns.mapHandler.BurntTiles = gridObject.grid.GetBlockLocationsWorldPosition(2).Count;
        
        MapHandlerObjectbttns.mapHandler.UpdateAverageStatistics();
        MapHandlerObjectbttns.mapHandler.SaveMap(MapHandlerObjectbttns.mapHandler.CurrentMapName);

        
        //remove people
        foreach (AILerp person in FindObjectsOfType<AILerp>())
        {
            Destroy(person.gameObject);
        }

        //remove fire
        foreach (Fire fire in FindObjectsOfType<Fire>())
        {
            gridObject.grid.SetValue(fire.transform.position, 0, floorPrefab);
            Destroy(fire.gameObject);
        }

        FireController f;
        GameObject instanceScript;
        instanceScript = GameObject.FindWithTag("startSim");
        if (instanceScript != null)
        {
            f = instanceScript.GetComponent<FireController>();
            f.StopFire();
        }

        bcontroller.EnableDelete();

        searchForSimulationEnd = false;

        Debug.Log("Simulation finished");
    }

    private void EnableFloorEditorMenu()
    {
        Button[] buttons = FindObjectsOfType<Button>();
        foreach (Button button in buttons)
        {
            //if the button is not a simulation control (e.g. pause, stop, speed change)
            if (!button.CompareTag("SimulationControls"))
            {
                button.interactable = true;
            }
            else
            {
                button.interactable = false;
            }
        }

        FindObjectOfType<Slider>().interactable = true;
    }

    private void DisableFloorEditorMenu()
    {
        Button[] buttons = FindObjectsOfType<Button>();
        foreach (Button button in buttons)
        {
            //if the button is not a simulation control (e.g. pause, stop, speed change)
            if (!button.CompareTag("SimulationControls"))
            {
                button.interactable = false;
            }
            else
            {
                button.interactable = true;
            }
        }

        FindObjectOfType<Slider>().interactable = false;
    }
}
