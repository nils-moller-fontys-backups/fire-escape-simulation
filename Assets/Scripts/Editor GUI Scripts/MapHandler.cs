﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class stores the values and from the current simulation, stats and changes in the floorplan. If you run
/// UpdateStats() the stats will be updated. When You call Savemap() everything is saved to a file.
/// </summary>
public class MapHandler //the class that handles the stuff between the map itself and the SaveSystem
{
    //Saving and loading of maps
    public int[,] mapObjects;
    public MapStats mapStatistics = new MapStats();
    public string CurrentMapName = "";
    public CGridScriptableObject grid;
    
     public MapHandler(CGridScriptableObject grid)
     {
        this.grid = grid;
     }
    

    //to keep track of the happenings in this simulation
    public int People = 0;
    public int Injured = 0;
    public int Deaths = 0;
    public int BurntTiles = 0;
    public double SimulationTime = 0;


    public void UpdateAverageStatistics()
    {
        try
        {
            //this checks if the map you just created is a new map, problem: Person may have saved before running once
            //if this maphandler has a name
            if (CurrentMapName != "")
            {
                int currentRuns = mapStatistics.NumberOfRuns;
                int newRuns = currentRuns++;

                mapStatistics.AveragenrOfPeople = ((mapStatistics.AveragenrOfPeople * currentRuns + People) / newRuns);
                mapStatistics.AverageOfInjured = ((mapStatistics.AverageOfInjured * currentRuns + Injured) / newRuns);
                mapStatistics.AverageOfDeaths = ((mapStatistics.AverageOfDeaths * currentRuns + Deaths) / newRuns);
                mapStatistics.AverageOfBurntTiles = ((mapStatistics.AverageOfBurntTiles * currentRuns + BurntTiles) / newRuns);
                mapStatistics.AverageSimulationTime = ((mapStatistics.AverageSimulationTime * currentRuns + SimulationTime) / newRuns);
                mapStatistics.NumberOfRuns = newRuns;
                Debug.Log("UpdateAvarageStatistics() did have a currentMapName");
            }
            //if this maphandler does not have a mapname
            else
            {
                mapStatistics.AveragenrOfPeople = People;
                mapStatistics.AverageOfInjured = Injured;
                mapStatistics.AverageOfDeaths = Deaths;
                mapStatistics.AverageOfBurntTiles = BurntTiles;
                mapStatistics.AverageSimulationTime = SimulationTime;
                mapStatistics.NumberOfRuns = 1;
                Debug.Log("UpdateAvarageStatistics() did not have a currentMapName");
            }
        }
        //set stats for this simulation back to zero
        finally
        {
            People = 0;
            Injured = 0;
            Deaths = 0;
            BurntTiles = 0;
            SimulationTime = 0;
        }
        Debug.Log("Updated average stats");
    }

    public void SaveMap(string mapname)
    {
        int[,] gridpositions = grid.grid.GridArray;
        mapObjects = gridpositions;
        CurrentMapName = mapname;
        SaveSystem.SaveMap(this, mapname);
        Debug.Log("Saved map");
    }

    /// <summary>
    /// Sets the local MapHandler fields to their loaded versions and builds the grid.
    /// </summary>
    /// <param name="mapname"></param>
    public void LoadMap(string mapname)
    {
        SaveData data = SaveSystem.LoadSaveData(mapname);
        mapObjects = data.mapObjects;
        mapStatistics = data.mapStats;
        CurrentMapName = mapname;

        //grid.grid.GridArray = mapObjects; //something wrong here
        for(int x = 0;  x < mapObjects.GetLength(0); x++)
        {
           for(int y = 0; y < mapObjects.GetLength(1); y++)
           {
                    //Resources.Load<GameObject>("wall");
                if (mapObjects[x, y] == 0)
                {
                    grid.grid.SetValue(x, y, 0, Resources.Load<GameObject>("floor"));
                }
                else if (mapObjects[x, y] == 1)
                {
                    grid.grid.SetValue(x, y, 1, Resources.Load<GameObject>("wall"));
                }
                else if (mapObjects[x, y] == 3)
                {
                    grid.grid.SetValue(x, y, 1, Resources.Load<GameObject>("wall"));
                    grid.grid.SetValue(x, y, 3, Resources.Load<GameObject>("window"));
                }
                else if (mapObjects[x, y] == 5)
                {
                    grid.grid.SetValue(x, y, 1, Resources.Load<GameObject>("wall"));
                    grid.grid.SetValue(x, y, 5, Resources.Load<GameObject>("EmergencyExit"));
                }
                else if (mapObjects[x, y] == 6)
                {
                    grid.grid.SetValue(x, y, 1, Resources.Load<GameObject>("wall"));
                    grid.grid.SetValue(x, y, 6, Resources.Load<GameObject>("door"));
                }
                // grid.grid.SetValue(x, y, grid.grid.GridArray, grid.grid.buildingblock);
            }
        }

        Debug.Log("Loaded map");
    }

    public void LoadStatistics(string mapName)
    {
        SaveData data = SaveSystem.LoadSaveData(mapName);
        mapObjects = data.mapObjects;
        mapStatistics = data.mapStats;
        CurrentMapName = mapName;
    }

    public void ResetCurrentSimulationData()
    {
        People = 0;
        Injured = 0;
        Deaths = 0;
        BurntTiles = 0;
        SimulationTime = 0;
        Debug.Log("Reset current sim data");
    }
}
